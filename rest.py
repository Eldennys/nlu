from flask import Flask, request, jsonify
from infer import config_file, model, phrase, predictions, opt, classes, preprocessor, s
from intent_classifier import infer

api = Flask(__name__)


@api.route("/company_data", method = ["GET"])
def data():
    dt = input("Enter Company ID: ")
    j = jsonify(dt)
    return j


@api.route("/question", method = ["GET"])
def qs():
    z = jsonify(phrase)
    return z


@api.route("/predictions", method = ["GET"])
def answer():
    pred = jsonify(predictions)
    # result
    res = jsonify(s)
    return pred, res

if __name == "__main__":
    api.run(debug=True)
